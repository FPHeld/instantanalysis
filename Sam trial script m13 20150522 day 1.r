# author: Holger Henneicke
# date: 2014-10-22
# title: analysis for the output of macro13 from expedata

# This script imports the output of macro 13 from expedata and calculates average,
# cummulative and auc results for various parameters. The script grabs a 12h long 
# light and a 12h long dark cycle from the original file as a bases for the analysis.
# All output parameters are calculated as either 'dark' or 'light' signifying the
# light and dark cycle in the animal housing facility (here: 6h to 18h).
# For every new dataset the dates will have to be adjusted when data is grabbed
# from the orignial file.
# 
# See right below for packages that are required:
#

#Sys.setenv(http_proxy_user = "ssolon:L4BbbjJT")
#Sys.setenv(http_proxy = "http://web-cache.usyd.edu.au:8080")

#install.packages("sfsmisc")
#install.packages("flux")
#install.packages("MASS")
#install.packages("gdata")
#install.packages("WriteXLS")


# set working directory
getwd()
setwd('C:\\Users\\ssolon\\Desktop\\')

library(gdata)

# read file into R
m13 <- read.csv('2015.05.11 c1 run1.csv')

# check dataset
str(m13)





################################
################################
###
###  CONVERTING DATE AND TIME AND SELCTION OF LIGHT AND DARK PHASES
###
################################
################################




# convert timestamp into date format including time
timestamp <- strptime(m13$Date_Time_1, format = "%d/%m/%Y %H:%M")
str(timestamp)
class(timestamp)

# check timestamp, diff should be 5 min
timestamp[2] - timestamp[1]

# integrate timestamp into m13
m13$Date_Time_1 <- timestamp
names(m13)[names(m13) == 'Date_Time_1'] <- 'timestamp'
str(m13)
m13$timestamp[2] - m13$timestamp[1]
m13$timestamp[length(timestamp)] - m13$timestamp[1]

### IF I WANTED TO DO MULTIPLE DARK AND LIGHT PERIODS CAN I JUST REPEAT THIS CODE? 
# selecting subset based on timestamp (here: day 1 light and dark phase) - for Sam dark 1 then light 1.
dark1 <- subset(m13,
                timestamp >= as.POSIXlt('2015-05-11 18:00:00') &
                timestamp <= as.POSIXlt('2015-05-12 06:00:00')
                )

str(dark1)


light1 <- subset(m13,
                timestamp >= as.POSIXlt('2015-05-12 06:00:00') &
                timestamp <= as.POSIXlt('2015-05-12 18:00:00')
                )

str(light1)





################################
################################
###
###  EE AUC AND BASELINE LIGHT1
###
################################
################################




# install and call flux package to run auc-function

library(flux)


####GET STUCK HERE.

# create time in min vector for 144 datapoints
time_min <- seq(0,((length(light1$timestamp))-1)*5, 5)

# calculate auc for EE cages 1 to 8 and store in vector EE_auc
EE_auc_light1 <- c(auc(time_min, light1$kcal_hr_1), 
                   auc(time_min, light1$kcal_hr_2), 
                   auc(time_min, light1$kcal_hr_3), 
                   auc(time_min, light1$kcal_hr_4), 
                   auc(time_min, light1$kcal_hr_5), 
                   auc(time_min, light1$kcal_hr_6), 
                   auc(time_min, light1$kcal_hr_7), 
                   auc(time_min, light1$kcal_hr_8)
                   )

EE_auc_light1


# install package sfsmisc to use integrate.xy, works great, possibly better than auc function
# intergrate.xy inegrates without spline and allows for easy setting of boundaries

library(sfsmisc)
integrate.xy(time_min, light1$kcal_hr_1, use.spline = FALSE)

# using inegrate.xy 20 min sections can now be easily used to identify baseline EE
# lower boundary should be a sequence of numbers between 0 and (715-20=695) 695 with intervals of 5
# lb = lower boundary (here: time 0)
# ub = upper boundary (here: lb+20min)
# the function should return the integral of a 20 min section
# location of the section depends on the input value for lower boudary
# lb here sequence between 0 min and 695 min with 5 min intervals  to cover the whole range

# BASELINE EE FOR CAGE 1
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_1,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c1 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 2
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_2,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c2 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 3
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_3,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c3 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 4
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_4,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c4 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 5
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_5,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c5 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 6
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_6,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c6 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 7
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_7,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c7 <- baselineEEintegral/20

# BASELINE EE FOR CAGE 8
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, light1$kcal_hr_8,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEElight1c8 <- baselineEEintegral/20

# bind baseline EE for light1 of individual cages together in 1 vector 
baseline_EE_light1 <- cbind(baselineEElight1c1, baselineEElight1c2, baselineEElight1c3, baselineEElight1c4, 
                            baselineEElight1c5, baselineEElight1c6, baselineEElight1c7, baselineEElight1c8
                            )

baseline_EE_light1

# housekeeping: remove 'old' variable
rm(baselineEElight1c1, baselineEElight1c2, baselineEElight1c3, baselineEElight1c4, 
   baselineEElight1c5, baselineEElight1c6, baselineEElight1c7, baselineEElight1c8)





################################
################################
###
###  EE AUC AND BASELINE DARK1
###
################################
################################




# install and call flux package to run auc-function

library(flux)

# create time in min vector for 144 datapoints
time_min <- seq(0,((length(dark1$timestamp))-1)*5, 5)

# calculate auc for EE cages 1 to 8 and store in vector EE_auc
EE_auc_dark1 <- c(auc(time_min, dark1$kcal_hr_1), 
                   auc(time_min, dark1$kcal_hr_2), 
                   auc(time_min, dark1$kcal_hr_3), 
                   auc(time_min, dark1$kcal_hr_4), 
                   auc(time_min, dark1$kcal_hr_5), 
                   auc(time_min, dark1$kcal_hr_6), 
                   auc(time_min, dark1$kcal_hr_7), 
                   auc(time_min, dark1$kcal_hr_8)
                   )

EE_auc_dark1


# install package sfsmisc to use integrate.xy, works great, possibly better than auc function
# intergrate.xy inegrates without spline and allows for easy setting of boundaries

library(sfsmisc)
integrate.xy(time_min, dark1$kcal_hr_1, use.spline = FALSE)

# using inegrate.xy 20 min sections can now be easily used to identify baseline EE
# lower boundary should be a sequence of numbers between 0 and (715-20=695) 695 with intervals of 5
# lb = lower boundary (here: time 0)
# ub = upper boundary (here: lb+20min)
# the function should return the integral of a 20 min section
# location of the section depends on the input value for lower boudary
# lb here sequence between 0 min and 695 min with 5 min intervals  to cover the whole range

# BASELINE EE FOR CAGE 1
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_1,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c1 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 2
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_2,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c2 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 3
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_3,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c3 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 4
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_4,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c4 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 5
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_5,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c5 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 6
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_6,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c6 <- baselineEEintegral/20


# BASELINE EE FOR CAGE 7
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_7,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c7 <- baselineEEintegral/20

# BASELINE EE FOR CAGE 8
partial.integral <- function(lb) {
                  ub <- lb+20
                  par.int <- integrate.xy(time_min, dark1$kcal_hr_8,lb ,ub , use.spline = FALSE)
                  return(par.int)
                                }

partial.integral(seq(0, 695, 5))

# lowest value
baselineEEintegral <- min(partial.integral(seq(0, 695, 5)))
# calculate average EE in kcal/h over the baseline 20 min interval
baselineEEdark1c8 <- baselineEEintegral/20

# bind baseline EE for dark1 of individual cages together in 1 vector 
baseline_EE_dark1 <- cbind(baselineEEdark1c1, baselineEEdark1c2, baselineEEdark1c3, baselineEEdark1c4, 
                            baselineEEdark1c5, baselineEEdark1c6, baselineEEdark1c7, baselineEEdark1c8
                            )

baseline_EE_dark1

# housekeeping: remove 'old' variable
rm(baselineEEdark1c1, baselineEEdark1c2, baselineEEdark1c3, baselineEEdark1c4, 
   baselineEEdark1c5, baselineEEdark1c6, baselineEEdark1c7, baselineEEdark1c8)
   
   
################################
################################
###
###  PEDMETERS AND ALLMETERS IN LIGHT1 AND DARK1
###
################################
################################


############
###  PEDMETERS
############


# calculating cumulative pedmeters for light1 
pedmeters_light1_c1 <- light1$pedmeters_1[length(light1$timestamp)] - light1$pedmeters_1[1]
pedmeters_light1_c2 <- light1$pedmeters_2[length(light1$timestamp)] - light1$pedmeters_2[1]
pedmeters_light1_c3 <- light1$pedmeters_3[length(light1$timestamp)] - light1$pedmeters_3[1]
pedmeters_light1_c4 <- light1$pedmeters_4[length(light1$timestamp)] - light1$pedmeters_4[1]
pedmeters_light1_c5 <- light1$pedmeters_5[length(light1$timestamp)] - light1$pedmeters_5[1]
pedmeters_light1_c6 <- light1$pedmeters_6[length(light1$timestamp)] - light1$pedmeters_6[1]
pedmeters_light1_c7 <- light1$pedmeters_7[length(light1$timestamp)] - light1$pedmeters_7[1]
pedmeters_light1_c8 <- light1$pedmeters_8[length(light1$timestamp)] - light1$pedmeters_8[1]
 
 
# bind cumulative pedmeters for light1 of individual cages together in 1 vector 
cum_pedmeters_light1 <- cbind(pedmeters_light1_c1, pedmeters_light1_c2, pedmeters_light1_c3, pedmeters_light1_c4, 
                              pedmeters_light1_c5, pedmeters_light1_c6, pedmeters_light1_c7, pedmeters_light1_c8
                              )

cum_pedmeters_light1

# housekeeping: remove 'old' variable
rm(pedmeters_light1_c1, pedmeters_light1_c2, pedmeters_light1_c3, pedmeters_light1_c4, 
   pedmeters_light1_c5, pedmeters_light1_c6, pedmeters_light1_c7, pedmeters_light1_c8)

# calculating cumulative pedmeters for dark1 
pedmeters_dark1_c1 <- dark1$pedmeters_1[length(dark1$timestamp)] - dark1$pedmeters_1[1]
pedmeters_dark1_c2 <- dark1$pedmeters_2[length(dark1$timestamp)] - dark1$pedmeters_2[1]
pedmeters_dark1_c3 <- dark1$pedmeters_3[length(dark1$timestamp)] - dark1$pedmeters_3[1]
pedmeters_dark1_c4 <- dark1$pedmeters_4[length(dark1$timestamp)] - dark1$pedmeters_4[1]
pedmeters_dark1_c5 <- dark1$pedmeters_5[length(dark1$timestamp)] - dark1$pedmeters_5[1]
pedmeters_dark1_c6 <- dark1$pedmeters_6[length(dark1$timestamp)] - dark1$pedmeters_6[1]
pedmeters_dark1_c7 <- dark1$pedmeters_7[length(dark1$timestamp)] - dark1$pedmeters_7[1]
pedmeters_dark1_c8 <- dark1$pedmeters_8[length(dark1$timestamp)] - dark1$pedmeters_8[1]
 

# bind cumulative pedmeters for dark1 of individual cages together in 1 vector 
cum_pedmeters_dark1 <- cbind(pedmeters_dark1_c1, pedmeters_dark1_c2, pedmeters_dark1_c3, pedmeters_dark1_c4, 
                             pedmeters_dark1_c5, pedmeters_dark1_c6, pedmeters_dark1_c7, pedmeters_dark1_c8
                             )

cum_pedmeters_dark1

# housekeeping: remove 'old' variable
rm(pedmeters_dark1_c1, pedmeters_dark1_c2, pedmeters_dark1_c3, pedmeters_dark1_c4, 
   pedmeters_dark1_c5, pedmeters_dark1_c6, pedmeters_dark1_c7, pedmeters_dark1_c8)


############
###  ALLMETERS
############


# calculating cumulative allmeters for light1 
allmeters_light1_c1 <- light1$allmeters_1[length(light1$timestamp)] - light1$allmeters_1[1]
allmeters_light1_c2 <- light1$allmeters_2[length(light1$timestamp)] - light1$allmeters_2[1]
allmeters_light1_c3 <- light1$allmeters_3[length(light1$timestamp)] - light1$allmeters_3[1]
allmeters_light1_c4 <- light1$allmeters_4[length(light1$timestamp)] - light1$allmeters_4[1]
allmeters_light1_c5 <- light1$allmeters_5[length(light1$timestamp)] - light1$allmeters_5[1]
allmeters_light1_c6 <- light1$allmeters_6[length(light1$timestamp)] - light1$allmeters_6[1]
allmeters_light1_c7 <- light1$allmeters_7[length(light1$timestamp)] - light1$allmeters_7[1]
allmeters_light1_c8 <- light1$allmeters_8[length(light1$timestamp)] - light1$allmeters_8[1]


# bind cumulative allmeters for light1 of individual cages together in 1 vector 
cum_allmeters_light1 <- cbind(allmeters_light1_c1, allmeters_light1_c2, allmeters_light1_c3, allmeters_light1_c4, 
                              allmeters_light1_c5, allmeters_light1_c6, allmeters_light1_c7, allmeters_light1_c8
                              )

cum_allmeters_light1

# housekeeping: remove 'old' variable
rm(allmeters_light1_c1, allmeters_light1_c2, allmeters_light1_c3, allmeters_light1_c4, 
   allmeters_light1_c5, allmeters_light1_c6, allmeters_light1_c7, allmeters_light1_c8)

# calculating cumulative allmeters for dark1 
allmeters_dark1_c1 <- dark1$allmeters_1[length(dark1$timestamp)] - dark1$allmeters_1[1]
allmeters_dark1_c2 <- dark1$allmeters_2[length(dark1$timestamp)] - dark1$allmeters_2[1]
allmeters_dark1_c3 <- dark1$allmeters_3[length(dark1$timestamp)] - dark1$allmeters_3[1]
allmeters_dark1_c4 <- dark1$allmeters_4[length(dark1$timestamp)] - dark1$allmeters_4[1]
allmeters_dark1_c5 <- dark1$allmeters_5[length(dark1$timestamp)] - dark1$allmeters_5[1]
allmeters_dark1_c6 <- dark1$allmeters_6[length(dark1$timestamp)] - dark1$allmeters_6[1]
allmeters_dark1_c7 <- dark1$allmeters_7[length(dark1$timestamp)] - dark1$allmeters_7[1]
allmeters_dark1_c8 <- dark1$allmeters_8[length(dark1$timestamp)] - dark1$allmeters_8[1]


# bind cumulative allmeters for dark1 of individual cages together in 1 vector 
cum_allmeters_dark1 <- cbind(allmeters_dark1_c1, allmeters_dark1_c2, allmeters_dark1_c3, allmeters_dark1_c4, 
                             allmeters_dark1_c5, allmeters_dark1_c6, allmeters_dark1_c7, allmeters_dark1_c8
                             )

cum_allmeters_dark1

# housekeeping: remove 'old' variable
rm(allmeters_dark1_c1, allmeters_dark1_c2, allmeters_dark1_c3, allmeters_dark1_c4, 
   allmeters_dark1_c5, allmeters_dark1_c6, allmeters_dark1_c7, allmeters_dark1_c8)





################################
################################
###
###     VO2max FOR LIGHT1
###
################################
################################




library(sfsmisc)
# using inegrate.xy 10 min sections can now easily be used to identify VO2max
# lower boundary should be a sequence of numbers between 0 and (715-10=705) 705 with intervals of 5
# lb = lower boundary (here: time 0)
# ub = upper boundary (here: lb+10min)
# the function should return the integral of a 10 min section
# location of the section depends on the input value for lower boudary
# lb here sequence between 0 min and 705 min with 5 min intervals to cover the whole range

# VO2max FOR CAGE 1
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_1,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c1 <- VO2integral/10


# VO2max FOR CAGE 2
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_2,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c2 <- VO2integral/10


# VO2max FOR CAGE 3
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_3,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c3 <- VO2integral/10


# VO2max FOR CAGE 4
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_4,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c4 <- VO2integral/10


# VO2max FOR CAGE 5
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_5,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c5 <- VO2integral/10


# VO2max FOR CAGE 6
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_6,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c6 <- VO2integral/10


# VO2max FOR CAGE 7
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_7,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c7 <- VO2integral/10


# VO2max FOR CAGE 8
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, light1$vo2_8,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxlight1c8 <- VO2integral/10

# bind VO2max for light1 of individual cages together in 1 vector 
VO2max_light1 <- cbind(VO2maxlight1c1, VO2maxlight1c2, VO2maxlight1c3, VO2maxlight1c4, 
                       VO2maxlight1c5, VO2maxlight1c6, VO2maxlight1c7, VO2maxlight1c8
)

VO2max_light1 

# housekeeping: remove 'old' variable
rm(VO2maxlight1c1, VO2maxlight1c2, VO2maxlight1c3, VO2maxlight1c4, 
   VO2maxlight1c5, VO2maxlight1c6, VO2maxlight1c7, VO2maxlight1c8, VO2integral)





################################
################################
###
###     VO2max FOR DARK1
###
################################
################################




library(sfsmisc)
# using inegrate.xy 10 min sections can now easily be used to identify VO2max
# lower boundary should be a sequence of numbers between 0 and (715-10=705) 705 with intervals of 5
# lb = lower boundary (here: time 0)
# ub = upper boundary (here: lb+10min)
# the function should return the integral of a 10 min section
# location of the section depends on the input value for lower boudary
# lb here sequence between 0 min and 705 min with 5 min intervals to cover the whole range

# VO2max FOR CAGE 1
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_1,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c1 <- VO2integral/10


# VO2max FOR CAGE 2
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_2,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c2 <- VO2integral/10


# VO2max FOR CAGE 3
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_3,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c3 <- VO2integral/10


# VO2max FOR CAGE 4
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_4,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c4 <- VO2integral/10


# VO2max FOR CAGE 5
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_5,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c5 <- VO2integral/10


# VO2max FOR CAGE 6
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_6,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c6 <- VO2integral/10


# VO2max FOR CAGE 7
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_7,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c7 <- VO2integral/10


# VO2max FOR CAGE 8
partial.integral <- function(lb) {
  ub <- lb+10
  par.int <- integrate.xy(time_min, dark1$vo2_8,lb ,ub , use.spline = FALSE)
  return(par.int)
}

partial.integral(seq(0, 705, 5))

# lowest value
VO2integral <- max(partial.integral(seq(0, 705, 5)))
# calculate average VO2 over the 10 min interval
VO2maxdark1c8 <- VO2integral/10

# bind VO2max for dark1 of individual cages together in 1 vector 
VO2max_dark1 <- cbind(VO2maxdark1c1, VO2maxdark1c2, VO2maxdark1c3, VO2maxdark1c4, 
                       VO2maxdark1c5, VO2maxdark1c6, VO2maxdark1c7, VO2maxdark1c8
)

VO2max_dark1 

# housekeeping: remove 'old' variable
rm(VO2maxdark1c1, VO2maxdark1c2, VO2maxdark1c3, VO2maxdark1c4, 
   VO2maxdark1c5, VO2maxdark1c6, VO2maxdark1c7, VO2maxdark1c8, VO2integral)





################################
################################
###
###     AVERAGE RQ, EE AND FOR LIGHT1 AND DARK1
###
################################
################################




# calculate average RQ for light1 for all 8 cages and store in vector
av_RQ_light1 <- c(mean(light1$rq_1, na.rm = TRUE),
                  mean(light1$rq_2, na.rm = TRUE),
                  mean(light1$rq_3, na.rm = TRUE),
                  mean(light1$rq_4, na.rm = TRUE),
                  mean(light1$rq_5, na.rm = TRUE),
                  mean(light1$rq_6, na.rm = TRUE),
                  mean(light1$rq_7, na.rm = TRUE),
                  mean(light1$rq_8, na.rm = TRUE)
                  )

# calculate average EE for light1 for all 8 cages and store in vector
av_EE_light1 <- c(mean(light1$kcal_hr_1, na.rm = TRUE),
                  mean(light1$kcal_hr_2, na.rm = TRUE),
                  mean(light1$kcal_hr_3, na.rm = TRUE),
                  mean(light1$kcal_hr_4, na.rm = TRUE),
                  mean(light1$kcal_hr_5, na.rm = TRUE),
                  mean(light1$kcal_hr_6, na.rm = TRUE),
                  mean(light1$kcal_hr_7, na.rm = TRUE),
                  mean(light1$kcal_hr_8, na.rm = TRUE)
                  )


# calculate average RQ for dark1 for all 8 cages and store in vector
av_RQ_dark1 <- c(mean(dark1$rq_1, na.rm = TRUE),
                 mean(dark1$rq_2, na.rm = TRUE),
                 mean(dark1$rq_3, na.rm = TRUE),
                 mean(dark1$rq_4, na.rm = TRUE),
                 mean(dark1$rq_5, na.rm = TRUE),
                 mean(dark1$rq_6, na.rm = TRUE),
                 mean(dark1$rq_7, na.rm = TRUE),
                 mean(dark1$rq_8, na.rm = TRUE)
                )

# calculate average EE for dark1 for all 8 cages and store in vector
av_EE_dark1 <- c(mean(dark1$kcal_hr_1, na.rm = TRUE),
                 mean(dark1$kcal_hr_2, na.rm = TRUE),
                 mean(dark1$kcal_hr_3, na.rm = TRUE),
                 mean(dark1$kcal_hr_4, na.rm = TRUE),
                 mean(dark1$kcal_hr_5, na.rm = TRUE),
                 mean(dark1$kcal_hr_6, na.rm = TRUE),
                 mean(dark1$kcal_hr_7, na.rm = TRUE),
                 mean(dark1$kcal_hr_8, na.rm = TRUE)
                )





################################
################################
###
###     AVERAGE VO2 AND VCO2 FOR LIGHT1 AND DARK1
###
################################
################################




# calculate average VO2 for light1 for all 8 cages and store in vector
av_VO2_light1 <- c(mean(light1$vo2_1, na.rm = TRUE),
                   mean(light1$vo2_2, na.rm = TRUE),
                   mean(light1$vo2_3, na.rm = TRUE),
                   mean(light1$vo2_4, na.rm = TRUE),
                   mean(light1$vo2_5, na.rm = TRUE),
                   mean(light1$vo2_6, na.rm = TRUE),
                   mean(light1$vo2_7, na.rm = TRUE),
                   mean(light1$vo2_8, na.rm = TRUE)
                  )

# calculate average VCO2 for light1 for all 8 cages and store in vector
av_VCO2_light1 <- c(mean(light1$vco2_1, na.rm = TRUE),
                    mean(light1$vco2_2, na.rm = TRUE),
                    mean(light1$vco2_3, na.rm = TRUE),
                    mean(light1$vco2_4, na.rm = TRUE),
                    mean(light1$vco2_5, na.rm = TRUE),
                    mean(light1$vco2_6, na.rm = TRUE),
                    mean(light1$vco2_7, na.rm = TRUE),
                    mean(light1$vco2_8, na.rm = TRUE)
                    )


# calculate average VO2 for dark1 for all 8 cages and store in vector
av_VO2_dark1 <- c(mean(dark1$vo2_1, na.rm = TRUE),
                  mean(dark1$vo2_2, na.rm = TRUE),
                  mean(dark1$vo2_3, na.rm = TRUE),
                  mean(dark1$vo2_4, na.rm = TRUE),
                  mean(dark1$vo2_5, na.rm = TRUE),
                  mean(dark1$vo2_6, na.rm = TRUE),
                  mean(dark1$vo2_7, na.rm = TRUE),
                  mean(dark1$vo2_8, na.rm = TRUE)
                  )

# calculate average VCO2 for dark1 for all 8 cages and store in vector
av_VCO2_dark1 <- c(mean(dark1$vco2_1, na.rm = TRUE),
                   mean(dark1$vco2_2, na.rm = TRUE),
                   mean(dark1$vco2_3, na.rm = TRUE),
                   mean(dark1$vco2_4, na.rm = TRUE),
                   mean(dark1$vco2_5, na.rm = TRUE),
                   mean(dark1$vco2_6, na.rm = TRUE),
                   mean(dark1$vco2_7, na.rm = TRUE),
                   mean(dark1$vco2_8, na.rm = TRUE)
                   )





################################
################################
###
###     AVERAGE BODYWEIGHT FOR LIGHT1 AND DARK1
###
################################
################################




# calculate average bodyweight for light1 for all 8 cages and store in vector
av_bw_light1 <- c(mean(light1$bodymass_1, na.rm = TRUE),
                  mean(light1$bodymass_2, na.rm = TRUE),
                  mean(light1$bodymass_3, na.rm = TRUE),
                  mean(light1$bodymass_4, na.rm = TRUE),
                  mean(light1$bodymass_5, na.rm = TRUE),
                  mean(light1$bodymass_6, na.rm = TRUE),
                  mean(light1$bodymass_7, na.rm = TRUE),
                  mean(light1$bodymass_8, na.rm = TRUE)
                  )

# calculate average bodyweight for dark1 for all 8 cages and store in vector
av_bw_dark1 <- c(mean(dark1$bodymass_1, na.rm = TRUE),
                 mean(dark1$bodymass_2, na.rm = TRUE),
                 mean(dark1$bodymass_3, na.rm = TRUE),
                 mean(dark1$bodymass_4, na.rm = TRUE),
                 mean(dark1$bodymass_5, na.rm = TRUE),
                 mean(dark1$bodymass_6, na.rm = TRUE),
                 mean(dark1$bodymass_7, na.rm = TRUE),
                 mean(dark1$bodymass_8, na.rm = TRUE)
                 )





################################
################################
###
###  FOOD INTAKE AND WATER INTAKE IN LIGHT1 AND DARK1
###
################################
################################




############
###  FOOD INTAKE
############


# calculating cumulative food intake for light1 
fi_light1_c1 <- light1$foodupa_1[length(light1$timestamp)] - light1$foodupa_1[1]
fi_light1_c2 <- light1$foodupa_2[length(light1$timestamp)] - light1$foodupa_2[1]
fi_light1_c3 <- light1$foodupa_3[length(light1$timestamp)] - light1$foodupa_3[1]
fi_light1_c4 <- light1$foodupa_4[length(light1$timestamp)] - light1$foodupa_4[1]
fi_light1_c5 <- light1$foodupa_5[length(light1$timestamp)] - light1$foodupa_5[1]
fi_light1_c6 <- light1$foodupa_6[length(light1$timestamp)] - light1$foodupa_6[1]
fi_light1_c7 <- light1$foodupa_7[length(light1$timestamp)] - light1$foodupa_7[1]
fi_light1_c8 <- light1$foodupa_8[length(light1$timestamp)] - light1$foodupa_8[1]


# bind cumulative food intake for light1 of individual cages together in 1 vector 
cum_fooduptake_light1 <- cbind(fi_light1_c1, fi_light1_c2, fi_light1_c3, fi_light1_c4, 
                               fi_light1_c5, fi_light1_c6, fi_light1_c7, fi_light1_c8
                               )

cum_fooduptake_light1

# housekeeping: remove 'old' variable
rm(fi_light1_c1, fi_light1_c2, fi_light1_c3, fi_light1_c4, 
   fi_light1_c5, fi_light1_c6, fi_light1_c7, fi_light1_c8)


# calculating cumulative food intake for dark1 
fi_dark1_c1 <- dark1$foodupa_1[length(dark1$timestamp)] - dark1$foodupa_1[1]
fi_dark1_c2 <- dark1$foodupa_2[length(dark1$timestamp)] - dark1$foodupa_2[1]
fi_dark1_c3 <- dark1$foodupa_3[length(dark1$timestamp)] - dark1$foodupa_3[1]
fi_dark1_c4 <- dark1$foodupa_4[length(dark1$timestamp)] - dark1$foodupa_4[1]
fi_dark1_c5 <- dark1$foodupa_5[length(dark1$timestamp)] - dark1$foodupa_5[1]
fi_dark1_c6 <- dark1$foodupa_6[length(dark1$timestamp)] - dark1$foodupa_6[1]
fi_dark1_c7 <- dark1$foodupa_7[length(dark1$timestamp)] - dark1$foodupa_7[1]
fi_dark1_c8 <- dark1$foodupa_8[length(dark1$timestamp)] - dark1$foodupa_8[1]


# bind cumulative food intake for dark1 of individual cages together in 1 vector 
cum_fooduptake_dark1 <- cbind(fi_dark1_c1, fi_dark1_c2, fi_dark1_c3, fi_dark1_c4,
                              fi_dark1_c5, fi_dark1_c6, fi_dark1_c7, fi_dark1_c8
                              )
     
cum_fooduptake_dark1

# housekeeping: remove 'old' variable
rm(fi_dark1_c1, fi_dark1_c2, fi_dark1_c3, fi_dark1_c4,
   fi_dark1_c5, fi_dark1_c6, fi_dark1_c7, fi_dark1_c8)


############
###  WTAER INTAKE
############


# calculating cumulative water uptake for light1 
wi_light1_c1 <- light1$waterupa_1[length(light1$timestamp)] - light1$waterupa_1[1]
wi_light1_c2 <- light1$waterupa_2[length(light1$timestamp)] - light1$waterupa_2[1]
wi_light1_c3 <- light1$waterupa_3[length(light1$timestamp)] - light1$waterupa_3[1]
wi_light1_c4 <- light1$waterupa_4[length(light1$timestamp)] - light1$waterupa_4[1]
wi_light1_c5 <- light1$waterupa_5[length(light1$timestamp)] - light1$waterupa_5[1]
wi_light1_c6 <- light1$waterupa_6[length(light1$timestamp)] - light1$waterupa_6[1]
wi_light1_c7 <- light1$waterupa_7[length(light1$timestamp)] - light1$waterupa_7[1]
wi_light1_c8 <- light1$waterupa_8[length(light1$timestamp)] - light1$waterupa_8[1]


# bind cumulative water uptake for light1 of individual cages together in 1 vector 
cum_wateruptake_light1 <- cbind(wi_light1_c1, wi_light1_c2, wi_light1_c3, wi_light1_c4, 
                                wi_light1_c5, wi_light1_c6, wi_light1_c7, wi_light1_c8
                                )

cum_wateruptake_light1

# housekeeping: remove 'old' variable
rm(wi_light1_c1, wi_light1_c2, wi_light1_c3, wi_light1_c4, 
   wi_light1_c5, wi_light1_c6, wi_light1_c7, wi_light1_c8)

# calculating cumulative water intake for dark1 
wi_dark1_c1 <- dark1$waterupa_1[length(dark1$timestamp)] - dark1$waterupa_1[1]
wi_dark1_c2 <- dark1$waterupa_2[length(dark1$timestamp)] - dark1$waterupa_2[1]
wi_dark1_c3 <- dark1$waterupa_3[length(dark1$timestamp)] - dark1$waterupa_3[1]
wi_dark1_c4 <- dark1$waterupa_4[length(dark1$timestamp)] - dark1$waterupa_4[1]
wi_dark1_c5 <- dark1$waterupa_5[length(dark1$timestamp)] - dark1$waterupa_5[1]
wi_dark1_c6 <- dark1$waterupa_6[length(dark1$timestamp)] - dark1$waterupa_6[1]
wi_dark1_c7 <- dark1$waterupa_7[length(dark1$timestamp)] - dark1$waterupa_7[1]
wi_dark1_c8 <- dark1$waterupa_8[length(dark1$timestamp)] - dark1$waterupa_8[1]


# bind cumulative water uptake for dark1 of individual cages together in 1 vector 
cum_wateruptake_dark1 <- cbind(wi_dark1_c1, wi_dark1_c2, wi_dark1_c3, wi_dark1_c4,
                               wi_dark1_c5, wi_dark1_c6, wi_dark1_c7, wi_dark1_c8
                               )

cum_wateruptake_dark1

# housekeeping: remove 'old' variable
rm(wi_dark1_c1, wi_dark1_c2, wi_dark1_c3, wi_dark1_c4,
   wi_dark1_c5, wi_dark1_c6, wi_dark1_c7, wi_dark1_c8)





################################
################################
###
###   COMBINE VECTORS INTO RESULTS TABLE AND EXPORT TO EXCEL FILE
###
################################
################################




# write data frames as vectors to combine with the data from other vectors
baseline_EE_light1 <- as.vector(baseline_EE_light1)
baseline_EE_dark1 <-  as.vector(baseline_EE_dark1)
cum_allmeters_light1 <- as.vector(cum_allmeters_light1)
cum_allmeters_dark1 <- as.vector(cum_allmeters_dark1)
cum_pedmeters_light1 <- as.vector(cum_pedmeters_light1)
cum_pedmeters_dark1 <- as.vector(cum_pedmeters_dark1)
cum_fooduptake_light1 <- as.vector(cum_fooduptake_light1)
cum_fooduptake_dark1 <- as.vector(cum_fooduptake_dark1)
cum_wateruptake_light1 <- as.vector(cum_wateruptake_light1)
cum_wateruptake_dark1 <- as.vector(cum_wateruptake_dark1)
VO2max_light1 <- as.vector(VO2max_light1)
VO2max_dark1 <- as.vector(VO2max_dark1)

# rowbind the results per cage (stored in vectors) together in a matrix
m13_results <- rbind(av_EE_light1, av_EE_dark1,
                     EE_auc_light1, EE_auc_dark1,
                     baseline_EE_light1, baseline_EE_dark1,
                     av_VO2_light1, av_VO2_dark1,
                     VO2max_light1, VO2max_dark1,
                     av_VCO2_light1, av_VCO2_dark1,
                     av_RQ_light1, av_RQ_dark1,
                     cum_pedmeters_light1, cum_pedmeters_dark1,
                     cum_allmeters_light1, cum_allmeters_dark1,
                     av_bw_light1, av_bw_dark1,
                     cum_fooduptake_light1, cum_fooduptake_dark1,
                     cum_wateruptake_light1, cum_wateruptake_dark1
                     )

m13_results

# create naming vectors
results_label <- c('cage_1', 'cage_2', 'cage_3', 'cage_4',
                   'cage_5', 'cage_6', 'cage_7', 'cage_8')

# apply labels to the results matrix
colnames(m13_results) <- results_label



#library(WriteXLS)
# write results file into XLS
#m13_results <- as.data.frame(m13_results)
#WriteXLS('m13_results', "m13_results.xls", BoldHeaderRow = TRUE, row.names = TRUE)

# Write results to CSV
write.csv(t(m13_results), file = "m13_results.csv")


#### end